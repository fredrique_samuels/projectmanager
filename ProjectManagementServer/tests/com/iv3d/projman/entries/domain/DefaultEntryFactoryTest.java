/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.projman.entries.domain;

import com.fred.projman.tests.MockAwareTestCase;
import com.iv3d.projman.entries.LogEntryFactory;
import com.iv3d.projman.entries.LogEntryParam;

public class DefaultEntryFactoryTest  extends MockAwareTestCase {
	private LogEntryFactory instance;

	@Override
	protected void setUp() throws Exception { 
		super.setUp();
		mockUtcDateIso8601("2016-01-02T13:00:00Z");
		mockIdGenerator("LOG_ENTRY_1");
		instance = new DefaultLogEntryFactory(clock, idGenerator);
	}
	
	public void testCreate() {
		LogEntryParam entry = instance.create("LOG_1", new String[]{"TagId"});
		
		assertNotNull(entry);
		assertEquals("LOG_ENTRY_1", entry.getId());
		assertEquals("LOG_1", entry.getProjectId());
		assertEquals("2016-01-02T13:00:00Z", entry.getDateCreated());
	    String[] tags = entry.getTagIds();
		assertEquals(1, tags.length);
		assertEquals("TagId", tags[0]);
		assertNull(entry.getMediaId());
	}	
}
