package com.iv3d.projman.dao;

import java.io.File;

import com.iv3d.common.utils.FileUtils;

import junit.framework.TestCase;

public class PersistedIdGeneratorTest extends TestCase {
	private final String DATA_ROOT = "iv3d.projman.data";
	private final String ID_GROUP = "idgroup";
	private final File DATA_ROOT_FILE = new File(DATA_ROOT);
	private final File ID_GROUP_FILE = new File(DATA_ROOT, "idgroup.idgen");
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		ID_GROUP_FILE.delete();
		DATA_ROOT_FILE.delete();
	}
	
	public void testConstruction() {
		new PersistedIdGenerator(ID_GROUP, DATA_ROOT_FILE);
	}
	
	public void testFileCreatedOnCreateID() {
		PersistedIdGenerator instance = new PersistedIdGenerator(ID_GROUP, DATA_ROOT_FILE);
		assertFalse(DATA_ROOT_FILE.exists());
		assertFalse(ID_GROUP_FILE.exists());
		instance.createId();
		assertTrue(DATA_ROOT_FILE.exists());
		assertTrue(ID_GROUP_FILE.exists());
	}
	
	public void testIdIncrementsAndSaved() {
		PersistedIdGenerator instance = new PersistedIdGenerator(ID_GROUP, DATA_ROOT_FILE);
		{
			String id = instance.createId();
			assertEquals("idgroup_1", id);
			assertEquals("1", FileUtils.contentsAsString(ID_GROUP_FILE));
		}
		
		{
			String id = instance.createId();
			assertEquals("idgroup_2", id);
			assertEquals("2", FileUtils.contentsAsString(ID_GROUP_FILE));
		}
		
		{
			String id = instance.createId();
			assertEquals("idgroup_3", id);
			assertEquals("3", FileUtils.contentsAsString(ID_GROUP_FILE));
		}
		
	}
}
