package com.iv3d.projman.tags.domain;

import com.iv3d.projman.tags.TagDao;
import com.iv3d.projman.tags.TagFactory;
import com.iv3d.projman.tags.TagManager;
import com.iv3d.projman.tags.TagParam;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;

public class DefaultTagManagerTest extends TestCase {
	private TagManager instance;
	@Mocked
	private TagFactory factory;
	@Mocked
	private TagDao dao;
	@Mocked
	TagParam tag;

	public void setUp() throws Exception {
		super.setUp();
		instance = new DefaultTagManager(factory, dao);
	}

	public void testCreateTag() {
		new NonStrictExpectations() {
			{
				factory.create("test_value");
				result = tag;
			}
		};
		
		final TagParam tag = instance.createTag("test_value");
		assertNotNull(tag);
		
		new Verifications() {
			
			{
				factory.create("test_value");
				times = 1;
				
				dao.upsert(tag);
				times = 1;
			}
		}; 
	}
	
	public void testUpsert() {
		instance.upsert(tag);
		
		new Verifications() {
			
			{
				dao.upsert(tag);
				times = 1;
			}
		};
		
	}public void testGet() {
		
		new NonStrictExpectations() {
			{
				dao.get("id");
				result = new TagParam[]{tag};
			}
		};
		
		TagParam[] pf = instance.get("id");
		assertEquals(1, pf.length);
		
		new Verifications() {
			{
				dao.get("id");
				times=1;
			}
		};
	}
	
	public void testGetIdList() {
		
		new NonStrictExpectations() {
			{
				dao.getIdsList();
				result = new String[]{};
			}
		};
		
		String[] pfs = instance.getIdsList();
		assertNotNull(pfs);
		
		new Verifications() {
			{
				dao.getIdsList();
				times=1;
			}
		};
	}
}
