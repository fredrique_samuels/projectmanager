package com.iv3d.projman;

import java.io.File;

import com.iv3d.common.IdGenerator;
import com.iv3d.common.date.SystemMilliSecSource;
import com.iv3d.common.date.UtcClock;
import com.iv3d.common.date.UtcSystemClock;
import com.iv3d.projman.dao.PersistedIdGenerator;
import com.iv3d.projman.entries.LogEntryDao;
import com.iv3d.projman.entries.LogEntryFactory;
import com.iv3d.projman.entries.LogEntryManager;
import com.iv3d.projman.entries.domain.DefaultLogEntryDao;
import com.iv3d.projman.entries.domain.DefaultLogEntryFactory;
import com.iv3d.projman.entries.domain.DefaultLogEntryManager;
import com.iv3d.projman.media.MediaDao;
import com.iv3d.projman.media.domain.DefaultMediaDao;

public class LogEntryManagerFactory {


	public LogEntryManager create(File dataRoot) {
		UtcClock clock = new UtcSystemClock(new SystemMilliSecSource());
		IdGenerator entryIdGenerator = new PersistedIdGenerator("entries", dataRoot);
		IdGenerator mediaIdGenerator = new PersistedIdGenerator("media", dataRoot);
		
		MediaDao mediaDao = new DefaultMediaDao(dataRoot, mediaIdGenerator);
		LogEntryDao entryDao = new DefaultLogEntryDao(dataRoot);
		LogEntryFactory factory = new DefaultLogEntryFactory(clock, entryIdGenerator);
		
		return new DefaultLogEntryManager(factory, entryDao, mediaDao);
	}
}
