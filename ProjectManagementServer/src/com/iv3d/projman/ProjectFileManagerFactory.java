package com.iv3d.projman;

import java.io.File;

import com.iv3d.common.IdGenerator;
import com.iv3d.common.date.SystemMilliSecSource;
import com.iv3d.common.date.UtcClock;
import com.iv3d.common.date.UtcSystemClock;
import com.iv3d.projman.dao.PersistedIdGenerator;
import com.iv3d.projman.projfile.ProjectFileDao;
import com.iv3d.projman.projfile.ProjectFileFactory;
import com.iv3d.projman.projfile.ProjectFileManager;
import com.iv3d.projman.projfile.domain.DefaultProjectFileDao;
import com.iv3d.projman.projfile.domain.DefaultProjectFileFactory;
import com.iv3d.projman.projfile.domain.DefaultProjectFileManager;

public class ProjectFileManagerFactory {
	public ProjectFileManager create(File dataRoot) {
		UtcClock clock = new UtcSystemClock(new SystemMilliSecSource());
		IdGenerator idGenerator = new PersistedIdGenerator("projfile", dataRoot);
		ProjectFileFactory projectFactory = new DefaultProjectFileFactory(clock, idGenerator);
		ProjectFileDao dao = new DefaultProjectFileDao(dataRoot);
		return new DefaultProjectFileManager(projectFactory, dao);
	}
}
