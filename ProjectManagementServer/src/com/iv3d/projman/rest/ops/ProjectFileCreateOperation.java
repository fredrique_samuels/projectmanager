package com.iv3d.projman.rest.ops;

import java.net.URI;

import com.iv3d.common.rest.RestClient;
import com.iv3d.common.utils.UriUtils;
import com.iv3d.projman.ProjectFile;
import com.iv3d.projman.projfile.ProjectFileParam;

public class ProjectFileCreateOperation extends GetOperation<ProjectFile, ProjectFileParam> {
	public ProjectFileCreateOperation(String domain, RestClient client) {
		super(domain, client, ProjectFileParam.class);
	}

	@Override
	URI createUrl() {
		return UriUtils.createForUrl(domain+"/iv3d/projman/projfile/create.jsp");
	}
}
