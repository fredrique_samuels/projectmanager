package com.iv3d.projman.rest.ops;

import java.net.URI;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import com.iv3d.common.rest.JsonResponseParser;
import com.iv3d.common.rest.Request;
import com.iv3d.common.rest.RestClient;
import com.iv3d.common.rest.RestOperation;

public abstract class GetOperation<R, P extends R> {

	protected final String domain;
	protected final RestClient client;
	private Class<P> paramType;
	
	public GetOperation(String domain, RestClient client, Class<P> paramType) {
		this.domain = domain;
		this.client = client;
		this.paramType = paramType;
	}

	public R execute() {
		return new RestOp<P>(new OpRequest(), paramType).execute(client);
	}

	
	class RestOp<T> extends RestOperation<T, String> {
		public RestOp(Request request, Class<T> type) {
			super(request, new JsonResponseParser<T>(type), String.class);
		}
	}
	
	class OpRequest implements Request {

		@Override
		public URI getUrl() {
			return createUrl();
		}

		@Override
		public HttpMethod getMethod() {
			return HttpMethod.GET;
		}

		@Override
		public HttpHeaders getHeaders() {
			return new HttpHeaders();
		}

		@Override
		public String getPayload() {
			return null;
		}
	}

	abstract URI createUrl();

}
