package com.iv3d.projman.rest;

import com.iv3d.common.rest.HttpRestClient;
import com.iv3d.common.rest.RestClient;
import com.iv3d.projman.ProjectFile;
import com.iv3d.projman.ProjectManager;
import com.iv3d.projman.rest.ops.ProjectFileCreateOperation;

public class HttpProjectManager implements ProjectManager {

	private RestClient restClient;
	private String domain;

	public HttpProjectManager(String host) {
        restClient = new HttpRestClient();
        domain = host;
        
	}

	@Override
	public ProjectFile createProjectFile() {
		return new ProjectFileCreateOperation(domain, restClient).execute();
	}

}
