package com.iv3d.projman.rest.server;
 
import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iv3d.common.utils.JsonUtils;
import com.iv3d.projman.ProjectFileManagerFactory;
import com.iv3d.projman.projfile.ProjectFileManager;
import com.iv3d.projman.projfile.ProjectFileParam;
import com.iv3d.projman.rest.server.domain.IdArray;
import com.iv3d.projman.rest.server.domain.ListResultContainer;
import com.iv3d.projman.rest.server.domain.ServerSettings;
import com.sun.istack.internal.logging.Logger;
 
/*
 * author: Crunchify.com
 * 
 */
 
@Controller
public class ProjectFileController extends BaseController {
	private static Logger logger = Logger.getLogger(ProjectFileController.class);
	private final ProjectFileManager projectFileManager;

	public ProjectFileController() {
		File dataRoot = new ServerSettings().getDataRoot();
		logger.info("Creating Project File Controller at  "+dataRoot.toURI().toString());
		projectFileManager = new ProjectFileManagerFactory().create(dataRoot);
	}
 
	@RequestMapping("/iv3d/projman/projfile/create")
	public ModelAndView createProject(
			@RequestParam(value="name") String name,
			@RequestParam(value="description", defaultValue="") String description,
	        HttpServletRequest request, 
	        HttpServletResponse response) {
		try {
			ProjectFileParam param = projectFileManager.createProject(name, description);
			String json = JsonUtils.writeToString(param);
			return new ModelAndView("value", "message", json);			
		} catch (Exception e) {
			return processError(request, e);
		}
	}
	
	@RequestMapping(value = "/iv3d/projman/projfile/save", method = RequestMethod.POST)
	public ModelAndView save(
			@RequestBody String body,
	        HttpServletRequest request, 
	        HttpServletResponse response) {
		try {
			ProjectFileParam param = JsonUtils.readFromString(body, ProjectFileParam.class);
			projectFileManager.upsert(param);
			return new ModelAndView("value", "message", createSuccessMessage(request));
		} catch (Exception e){
			return processError(request, e);
		}
	}
	 
	@RequestMapping(value = "/iv3d/projman/projfile/get", method = RequestMethod.POST)
	public ModelAndView get(
			@RequestBody String body,
	        HttpServletRequest request, 
	        HttpServletResponse response) {
		try {
			IdArray idArray = JsonUtils.readFromString(body, IdArray.class);
			ProjectFileParam[] params = projectFileManager.get(idArray.getIds());
			String json = JsonUtils.writeToString(new ListResultContainer<>(params));
			return new ModelAndView("value", "message", json);
		} catch (Exception e){
			return processError(request, e);
		}
	}
	
	@RequestMapping(value = "/iv3d/projman/projfile/getIds", method = RequestMethod.GET)
	public ModelAndView getIds(
	        HttpServletRequest request, 
	        HttpServletResponse response) {
		try {
			String[] params = projectFileManager.getIdsList();
			String json = JsonUtils.writeToString(new IdArray(params));
			return new ModelAndView("value", "message", json);
		} catch (Exception e){
			return processError(request, e);
		}
	}
	
}