package com.iv3d.projman.rest.server.domain;

public class ListResultContainer<T> {
	private T[] results;
	
	public ListResultContainer(T[] results) {
		this.results = results;
	}
	
	public ListResultContainer() {
	}

	public T[] getResults() {
		return results;
	}

	public void setResults(T[] results) {
		this.results = results;
	}
	
}
