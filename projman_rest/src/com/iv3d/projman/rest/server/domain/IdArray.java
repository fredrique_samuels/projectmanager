package com.iv3d.projman.rest.server.domain;

public class IdArray {
	private String[] ids;

	public IdArray() {
	}
	
	public IdArray(String[] ids) {
		this.ids = ids;
	}

	public String[] getIds() {
		return ids;
	}

	public void setIds(String[] ids) {
		this.ids = ids;
	}

}
