package com.iv3d.projman.rest.server;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;

import com.iv3d.common.utils.ExceptionUtils;
import com.sun.istack.internal.logging.Logger;

public class BaseController {
	private static Logger logger = Logger.getLogger(BaseController.class);

	protected final String createFailureMessage(HttpServletRequest request) {
		return "{\"status\":\"failed\"}";
	}
	
	protected final String createSuccessMessage(HttpServletRequest request) {
		return "{\"status\":\"succeeded\"}";
	}

	protected final ModelAndView processError(HttpServletRequest request, Exception e) {
		logger.warning(ExceptionUtils.getStackTrace(e));
		return new ModelAndView("value", "message", createFailureMessage(request));
	}
}
