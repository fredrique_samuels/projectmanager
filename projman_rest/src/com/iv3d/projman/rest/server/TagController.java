package com.iv3d.projman.rest.server;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iv3d.common.utils.JsonUtils;
import com.iv3d.projman.TagManagerFactory;
import com.iv3d.projman.rest.server.domain.IdArray;
import com.iv3d.projman.rest.server.domain.ListResultContainer;
import com.iv3d.projman.rest.server.domain.ServerSettings;
import com.iv3d.projman.tags.TagManager;
import com.iv3d.projman.tags.TagParam;
import com.sun.istack.internal.logging.Logger;

@Controller
public class TagController extends BaseController {
	private static Logger logger = Logger.getLogger(TagController.class);
	private final TagManager tagManager;

	public TagController() {
		File dataRoot = new ServerSettings().getDataRoot();
		logger.info("Creating Project File Controller at  "+dataRoot.toURI().toString());
		tagManager = new TagManagerFactory().create(dataRoot);
	}
 
	@RequestMapping("/iv3d/projman/tag/create")
	public ModelAndView createProject(
			@RequestParam(value="value") String value,
	        HttpServletRequest request, 
	        HttpServletResponse response) {
		try {
			TagParam param = tagManager.createTag(value);
			String json = JsonUtils.writeToString(param);
			return new ModelAndView("value", "message", json);			
		} catch (Exception e) {
			return processError(request, e);
		}
	}
	
	@RequestMapping(value = "/iv3d/projman/tag/save", method = RequestMethod.POST)
	public ModelAndView save(
			@RequestBody String body,
	        HttpServletRequest request, 
	        HttpServletResponse response) {
		try {
			TagParam param = JsonUtils.readFromString(body, TagParam.class);
			tagManager.upsert(param);
			return new ModelAndView("value", "message", createSuccessMessage(request));
		} catch (Exception e){
			return processError(request, e);
		}
	}
	 
	@RequestMapping(value = "/iv3d/projman/tag/get", method = RequestMethod.POST)
	public ModelAndView get(
			@RequestBody String body,
	        HttpServletRequest request, 
	        HttpServletResponse response) {
		try {
			IdArray idArray = JsonUtils.readFromString(body, IdArray.class);
			TagParam[] params = tagManager.get(idArray.getIds());
			String json = JsonUtils.writeToString(new ListResultContainer<>(params));
			return new ModelAndView("value", "message", json);
		} catch (Exception e){
			return processError(request, e);
		}
	}
	
	@RequestMapping(value = "/iv3d/projman/tag/getIds", method = RequestMethod.GET)
	public ModelAndView getIds(
	        HttpServletRequest request, 
	        HttpServletResponse response) {
		try {
			String[] params = tagManager.getIdsList();
			String json = JsonUtils.writeToString(new IdArray(params));
			return new ModelAndView("value", "message", json);
		} catch (Exception e){
			return processError(request, e);
		}
	}
}
