package com.iv3d.projman.rest.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.iv3d.common.utils.JsonUtils;
import com.iv3d.projman.LogEntryManagerFactory;
import com.iv3d.projman.entries.LogEntryManager;
import com.iv3d.projman.entries.LogEntryParam;
import com.iv3d.projman.rest.server.domain.ListResultContainer;
import com.iv3d.projman.rest.server.domain.ServerSettings;
import com.sun.istack.internal.logging.Logger;

@Controller
public class EntryController extends BaseController {
	private static Logger logger = Logger.getLogger(TagController.class);
	private final LogEntryManager logEntryManager;

	public EntryController() {
		File dataRoot = new ServerSettings().getDataRoot();
		logger.info("Creating Project File Controller at  " + dataRoot.toURI().toString());
		logEntryManager = new LogEntryManagerFactory().create(dataRoot);
	}

	@RequestMapping("/iv3d/projman/entries/upload_form")
	public ModelAndView createProject(HttpServletRequest request, HttpServletResponse response) {

		return new ModelAndView("upload_form");
	}

	@RequestMapping(method = RequestMethod.POST, value = "/iv3d/projman/entries/upload")
	public ModelAndView handleFileUpload(@RequestParam("projectId") String projectId,
			@RequestParam("tagIds") String tagIds, @RequestParam("mediaType") String mediaType,
			@RequestParam("file") MultipartFile file, HttpServletRequest request) {

		if (!file.isEmpty()) {
			try {
				LogEntryParam param = logEntryManager.save(projectId, tagIds.split(","), mediaType,
						file.getInputStream());
				return new ModelAndView("value", "message", JsonUtils.writeToString(param));
			} catch (Exception e) {
				return new ModelAndView("value", "message", createFailureMessage(request));
			}
		}
		return new ModelAndView("value", "message", createFailureMessage(request));
	}

	@RequestMapping(method = RequestMethod.GET, value = "/iv3d/projman/entries/get")
	public ModelAndView getEntries(@RequestParam("projectId") String projectId, HttpServletRequest request) {

		try {
			LogEntryParam[] params = logEntryManager.getLogs(projectId);
			return new ModelAndView("value", "message", JsonUtils.writeToString(new ListResultContainer<>(params)));
		} catch (Exception e) {
			return new ModelAndView("value", "message", createFailureMessage(request));
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/iv3d/projman/entries/media/get")
	public ModelAndView getMedia(
			@RequestParam("mediaId") String mediaId,
	        HttpServletRequest request, HttpServletResponse response) {
		
		try {
			File downloadFile = logEntryManager.getMedia(mediaId);
			
	        ServletContext context = request.getServletContext();
	        String appPath = context.getRealPath("");
	        System.out.println("appPath = " + appPath);
			FileInputStream inputStream = new FileInputStream(downloadFile);
			String mimeType = context.getMimeType(downloadFile.getPath());
			if (mimeType == null) {
				mimeType = "application/octet-stream";
			}
            System.out.println("MIME type: " + mimeType);
            response.setContentType(mimeType);
			response.setContentLength((int) downloadFile.length());
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"",downloadFile.getName());
			response.setHeader(headerKey, headerValue);
			
			OutputStream outStream = response.getOutputStream();

			FileCopyUtils.copy(inputStream, outStream);
	 
			inputStream.close();
			outStream.close();
		} catch (Exception e) {
			return new ModelAndView("value", "message", createFailureMessage(request));
		}
		return null;
	}

	// @RequestMapping("/iv3d/projman/tag/create")
	// public ModelAndView createProject(
	// @RequestParam(value="value") String value,
	// HttpServletRequest request,
	// HttpServletResponse response) {
	// try {
	// TagParam param = tagManager.createTag(value);
	// String json = JsonUtils.writeToString(param);
	// return new ModelAndView("value", "message", json);
	// } catch (Exception e) {
	// return processError(request, e);
	// }
	// }
	//
	// @RequestMapping(value = "/iv3d/projman/tag/save", method =
	// RequestMethod.POST)
	// public ModelAndView save(
	// @RequestBody String body,
	// HttpServletRequest request,
	// HttpServletResponse response) {
	// try {
	// TagParam param = JsonUtils.readFromString(body, TagParam.class);
	// tagManager.upsert(param);
	// return new ModelAndView("value", "message",
	// createSuccessMessage(request));
	// } catch (Exception e){
	// return processError(request, e);
	// }
	// }
	//
	// @RequestMapping(value = "/iv3d/projman/tag/get", method =
	// RequestMethod.POST)
	// public ModelAndView get(
	// @RequestBody String body,
	// HttpServletRequest request,
	// HttpServletResponse response) {
	// try {
	// IdArray idArray = JsonUtils.readFromString(body, IdArray.class);
	// TagParam[] params = tagManager.get(idArray.getIds());
	// String json = JsonUtils.writeToString(new ListResultContainer<>(params));
	// return new ModelAndView("value", "message", json);
	// } catch (Exception e){
	// return processError(request, e);
	// }
	// }
	//
	// @RequestMapping(value = "/iv3d/projman/tag/getIds", method =
	// RequestMethod.GET)
	// public ModelAndView getIds(
	// HttpServletRequest request,
	// HttpServletResponse response) {
	// try {
	// String[] params = tagManager.getIdsList();
	// String json = JsonUtils.writeToString(new IdArray(params));
	// return new ModelAndView("value", "message", json);
	// } catch (Exception e){
	// return processError(request, e);
	// }
	// }
}
