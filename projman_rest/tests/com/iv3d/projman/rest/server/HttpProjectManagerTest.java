package com.iv3d.projman.rest.server;

import java.net.URI;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import com.iv3d.common.rest.HttpRestClient;
import com.iv3d.common.rest.Request;
import com.iv3d.common.utils.UriUtils;
import com.iv3d.projman.ProjectFile;
import com.iv3d.projman.ProjectManager;
import com.iv3d.projman.rest.HttpProjectManager;
import com.sun.jndi.toolkit.url.UrlUtil;

import junit.framework.TestCase;

public class HttpProjectManagerTest extends TestCase {
	private static final String HOST = "http://localhost:8080/projecman_rest";
	private ProjectManager instance;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		instance = new HttpProjectManager(HOST);
	}
	
	public void testCreateProject() {
		ProjectFile projectFile = instance.createProjectFile();
		assertNotNull(projectFile);
	}
	
	public void testSaveProject() {
		HttpRestClient client = new HttpRestClient();
		client.execute(new Request() {
			
			@Override
			public URI getUrl() {
				return UriUtils.createForUrl(HOST+"/test");
			}
			
			@Override
			public String getPayload() {
				return "Test Payload";
			}
			
			@Override
			public HttpMethod getMethod() {
				return HttpMethod.POST;
			}
			
			@Override
			public HttpHeaders getHeaders() {
				return new HttpHeaders();
			}
		}, String.class);
	}
}
