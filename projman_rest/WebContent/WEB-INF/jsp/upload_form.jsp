<html xmlns:th="http://www.thymeleaf.org">
<body>

	<div>
		<form method="POST" enctype="multipart/form-data" action="/projecman_rest/iv3d/projman/entries/upload.jsp">
			<table>
				<tr><td>File to upload:</td><td><input type="file" name="file" /></td></tr>
				<tr><td>Project id:</td><td><input type="text" name="projectId" /></td></tr>
				<tr><td>Tag ids:</td><td><input type="text" name="tagIds" /></td></tr>
				<tr><td>Media Type:</td><td><input type="text" name="mediaType" /></td></tr>
				<tr><td></td><td><input type="submit" value="Upload" /></td></tr>
			</table>
		</form>
	</div>

</body>
</html>